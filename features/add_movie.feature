Feature: display list of movies and let one add new ones
  As a rotten potatoes user
  So that I can contribute with information about my favorite movies
  I want to add new movies to the public list of them

Scenario: add a new movie to an initially empty list
  Given the list of movies is empty
  When I add "The Lion King", released on "2019-07-19", with rating "G"
  Then a total of 1 movie should be shown in the list